package model.data_structures;

public class NodoEncadenado<T> {

	private NodoEncadenado<T> siguiente;
	
	private T elemento;
	
	public NodoEncadenado(T elemento)
	{
		this.elemento = elemento;
		siguiente = null;
	}
	
	public NodoEncadenado<T> agregarElementoFinal(T elemento) {
		if(siguiente == null)
		{
			siguiente = new NodoEncadenado<T>(elemento);
			return siguiente;
		}
		else
		{
			return siguiente.agregarElementoFinal(elemento);
			
		}
	}
	
	public NodoEncadenado<T> darSiguiente()
	{
		return siguiente;
	}
	public T darElemento()
	{
		return elemento;
	}

	public T darElementoEn(int posicion) {
		if(posicion == 0)
		{
			return elemento;
		}
		else
		{
			if(siguiente!= null)
			{
				return siguiente.darElementoEn(posicion-1);
			}
			throw new IndexOutOfBoundsException("No existe el elemento buscado");
		}
	}

	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		if(siguiente == null)
		{
			return 1;
		}
		else
		{
			return 1+siguiente.darNumeroElementos();
		}
		
	}
}


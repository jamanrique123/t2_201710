package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectStreamException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;

	public final static int PRIMERO_DISPONIBLE = 1950;

	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		// TODO Auto-generated method stub
		try
		{
			ListaEncadenada<VOPelicula> listaPeliculas = new ListaEncadenada<VOPelicula>();

			BufferedReader br = new BufferedReader(new FileReader(archivoPeliculas));

			String linea = br.readLine();

			while (linea != null)
			{					
				String[] datos = linea.split(",");

				ListaEncadenada<String> generos = new ListaEncadenada<String>();
				int agnoPelicula = 0;
				String titulo = "";

				String[] generosRaw;
				generosRaw = datos[datos.length -1].split("\\|");

				for (int i = 0; i < generosRaw.length; i++)
				{
					generos.agregarElementoFinal(generosRaw[i]);
				}

				//Caso especial a�o y nombre
				if (datos.length > 3)
				{
					String nombreBueno = "";				

					for (int i = 1; i < datos.length - 1; i++)
					{
						if (i != 1)
						{
							nombreBueno = nombreBueno + "," + datos[i];
						}
						else
						{
							nombreBueno = nombreBueno + datos[i];
						}
					}

					nombreBueno = nombreBueno.replaceAll("\"", "");

					String[] agnoArray = nombreBueno.split("\\(");
					String agnoString = agnoArray[ agnoArray.length-1 ].replaceAll("\\)", "");

					agnoString = agnoString.split("-")[0];

					try 
					{
						agnoPelicula = Integer.parseInt(agnoString);
					}
					catch (NumberFormatException e)
					{
						System.out.println("No tiene a�o: " + agnoString);
						agnoPelicula = -1;
					}



					if (nombreBueno.contains(" (19") || nombreBueno.contains(" (20"))
						titulo = nombreBueno.substring(0, nombreBueno.length() - 7);				
					else
						titulo = nombreBueno;

					System.out.println(titulo+ "\n" + agnoPelicula  + "\n" + generos);
				}
				else //es normal
				{
					String datoTitulo = datos[1].trim();
					if(datoTitulo.startsWith("\""))
					{
						agnoPelicula = Integer.parseInt(datoTitulo.substring(datoTitulo.length() -6, datoTitulo.length() -2));
					}
					else
					{
						String substringSupuestoA�o = datoTitulo.substring(datoTitulo.length() -5, datoTitulo.length() -1);
						try
						{
							agnoPelicula = Integer.parseInt(substringSupuestoA�o);
							titulo = datoTitulo.substring(0, datoTitulo.length() -6);
						}
						catch(NumberFormatException e)
						{
							agnoPelicula = -1;
							titulo = datos[1];
						}
					}
					System.out.println(titulo+"\n"+agnoPelicula+"\n"+generos);
				}

				//Con la linea completamente parseada, croe el VO pel�cula

				VOPelicula voPel = new VOPelicula();

				voPel.setAgnoPublicacion(agnoPelicula);
				voPel.setGenerosAsociados(generos);
				voPel.setTitulo(titulo);

				listaPeliculas.agregarElementoFinal(voPel);

				linea = br.readLine();
			}

			br.close();

			misPeliculas = listaPeliculas;
			
		}
		catch (IOException e)
		{
			System.out.println("File not found");
			e.printStackTrace();
		}

		//ahora que ya tengo todas las pel�culas en una lista, hago la de los a�os
		
		ListaDobleEncadenada<VOAgnoPelicula> listaAgnos = new ListaDobleEncadenada<VOAgnoPelicula>();
		
		//Este recorrido inicia desde el primer a�o de los valores hasta el a�o actual
		for (int i = PRIMERO_DISPONIBLE; i <= 2017; i++ )
		{
			VOAgnoPelicula agnoActual = new VOAgnoPelicula();
						
			int numAgnoActual = i;
			agnoActual.setAgno(numAgnoActual);
			
			Iterator<VOPelicula> iter = misPeliculas.iterator();
			ListaEncadenada<VOPelicula> listaPeliculas = new ListaEncadenada<VOPelicula>();
			
			while (iter.hasNext())
			{
				VOPelicula peliculaActual = iter.next();
				
				if (peliculaActual.getAgnoPublicacion() == numAgnoActual)
					listaPeliculas.agregarElementoFinal(peliculaActual);				
			}
			
			agnoActual.setPeliculas(listaPeliculas);
			
			listaAgnos.agregarElementoFinal(agnoActual);
		}
		
		peliculasAgno = listaAgnos;

	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub

		Iterator<VOPelicula> iter = misPeliculas.iterator();
		ListaDobleEncadenada<VOPelicula> listaPeliculas = new ListaDobleEncadenada<VOPelicula>();

		while (iter.hasNext())
		{
			VOPelicula actual = iter.next();

			if (actual.getTitulo().toLowerCase().contains(busqueda))
				listaPeliculas.agregarElementoFinal(actual);
		}

		return listaPeliculas;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		// TODO Auto-generated method stub

		boolean encontreAgno = false;
		
		while (!encontreAgno)
		{
			VOAgnoPelicula actual = peliculasAgno.darElementoPosicionActual();
			
			System.out.println(actual);
			
			if (actual.getAgno() == agno)
				encontreAgno = true;
			else //si no puedo avanzar mas, ya acabe el recorrido y no encontr� nada, entonces cierro el ciclo
				encontreAgno = !peliculasAgno.avanzarSiguientePosicion();
		}
		
		return peliculasAgno.darElementoPosicionActual().getPeliculas();
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub

		peliculasAgno.avanzarSiguientePosicion();

		return peliculasAgno.darElementoPosicionActual();
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		
		peliculasAgno.retrocederPosicionAnterior();
		
		return peliculasAgno.darElementoPosicionActual();
	}	
}
